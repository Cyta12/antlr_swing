package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class MyTreeParser extends TreeParser {

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(Integer tmp_int) { System.out.println(tmp_int.toString());}

    protected Integer power(Integer base, Integer exponent) {return (int) Math.pow(base, exponent);}

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
